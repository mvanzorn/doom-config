(setq user-full-name "Your Name")

(setq user-mail-address "name@example.com")

(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")
                         ("melpa" . "http://melpa.org/packages/")))

(defun ht-dar/advice-unadvice (sym)
  "Remove all advices from symbol SYM."
  (interactive "aFunction symbol: ")
  (advice-mapc (lambda (advice _props) (advice-remove sym advice)) sym))

(load-theme 'doom-spacegrey t)

(add-hook 'doom-init-ui-hook #'doom-init-theme-h)
(remove-hook 'after-make-frame-functions #'doom-init-theme-h)

(setq frame-title-format "%b | λI")

(add-to-list 'default-frame-alist
             '(font . "SF Mono 11"))

(setq +doom-dashboard-name "λI:home")

(setq +doom-dashboard-banner-dir "~/../other")

(setq +doom-dashboard-banner-file "ai-splash-small.png")

;; remove footer & 'loaded' information
(setq +doom-dashboard-functions
      '(doom-dashboard-widget-banner
        doom-dashboard-widget-shortmenu))

(after! persp-mode (setq persp-emacsclient-init-frame-behaviour-override -1))

(setq projectile-git-submodule-command nil)

(when IS-WINDOWS
  (setq inhibit-compacting-font-caches t) ; speed-up on windows
  )

(when IS-WINDOWS
  (defvar +propositum/last-w32-notification-id nil
  "Stores the ID value of the last notification.
    Required by `w32-notification-notify' to send a successive notification.")

(defun +propositum/send-desktop-notification (message)
  "Sends a 'server initialised' notification to the windows desktop using `w32-notification-notify'"

  ;; delete any previous notification first (required to launch new notifications)
  (when +propositum/last-w32-notification-id
    (w32-notification-close +propositum/last-w32-notification-id))

  ;; send the new notificaiton, storing the id in the global var
  (setq +propositum/last-w32-notification-id
        (w32-notification-notify
         :type 'info
         :title "ΛSSIST"
         :body message))))

(when IS-WINDOWS

  (defun +propositum/daemon-open-client-frame ()
  "open a client frame from within emacs, if emacs was started within daemon mode"

  (when (daemonp) ;; only run if emacs process started as a daemon (i.e. runemacs --daemon)
    (make-process ; asynchronously launches emacsclientw.exe
     :name "emacsclientw"
     :buffer nil
     :command (list (format "%s/bin/emacsclientw.exe" (getenv "emacs_dir")) "-c") ; use directly, not the shim
     :noquery t))) ; dont ask about running process when exiting

;; add to the hook that runs after emacs has loaded
(add-hook 'emacs-startup-hook #'+propositum/daemon-open-client-frame t))

(when IS-WINDOWS
  (add-to-list 'exec-path (concat (getenv "propositumDrive") "/apps/cmder-full/current/vendor/git-for-windows/usr/bin"))
;  (add-to-list 'exec-path (concat (getenv "propositumDrive") "/apps/cmder-full/current/vendor/git-for-windows/bin")) ; for git.exe
;  (add-to-list 'exec-path (concat (getenv "propositumDrive") "/apps/cmder-full/current/vendor/git-for-windows/mingw64/bin")) ; for mingw64 tools
  )

(when IS-MAC
  (setq ns-use-thin-smoothing t) ; better looking font rendering
  (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t)) ; support macos integrated titlebar
  (add-to-list 'default-frame-alist '(ns-appearance . dark)) ; support macos dark mode
  (setq ns-right-alternate-modifier (quote none)) ; un-hijack right alt to do symbols
  )

(defgroup propositum nil
  "task & information management system built around `org-mode'."
  :group 'emacs)

(defconst propositum-projects-directory "~/projects" "default directory where projects are created")
(defconst propositum-expanded-projects-directory (expand-file-name propositum-projects-directory "same as `propositum-projects-directory' but any aliases (e.g. '~') are expanded"))

(map!
     (:after projectile
       :leader :prefix ("1" . "propositum")
       :leader :prefix ("1n" . "new propositum")
       :desc "New propositum project (barebones)" :g "b" #'+propositum/new-project
       :desc "New propositum project with templates" :g "t" #'+propositum/new-project-with-templates
       :leader :prefix ("1d" . "dev project")
       :desc "New dev project (barebones)" :g "db" #'+propositum/new-dev-project
       :desc "New dev project with templates" :g "dt" #'+propositum/new-dev-project-with-templates
 ))

(after! yasnippet

  ;; set path to file templates
  (setq +file-templates-dir
        "~/.doom.d/file-templates")
     )

(defun +propositum/new-project (proj-name)
  "Creates a new propositum project in the `projectile-project-search-path`."
  (interactive "sNew propositum project: ")
  (save-current-buffer
    (let
        ((proj-path (concat (car projectile-project-search-path) proj-name)))
         ;(default-directory (file-name-as-directory proj-path))) ; add trailing slash as required by `default-directory'
      (make-directory proj-path)
      (write-region "" 'nil (concat proj-path "/.projectile"))
      (write-region "" 'nil (concat proj-path "/.propositum"))
      (add-dir-local-variable 'nil 'projectile-project-name proj-name) ; as sometimes folder name may be different / not descriptive
      (add-dir-local-variable 'nil 'project-bg-colour (+propositum/html-colour-code-from-string proj-name)) ; unique background colour generated from project name
      (write-file (concat proj-path "/.dir-locals.el"))
      (kill-buffer)
      (message "Created propositum project: '%s' in %s" proj-name proj-path))))

(defconst propositum-project-templates '((org-mode . "__propositum")
                                         (org-mode . "__something"))
  "list of yasnippet templates (mode . snippet-name) which should be created for a project")

(defun +propositum/create-project-templates (&opt loc)
  "Insert a license file template into the current file."
  (interactive)
  (require 'yasnippet)
  (let ((templates
         (let (yas-choose-tables-first ; avoid prompts
               yas-choose-keys-first)
           (cl-loop for tpl in (yas--all-templates (yas--get-snippet-tables 'text-mode))
                    for uuid = (yas--template-uuid tpl)
                    if (string-prefix-p "__license-" uuid)
                    collect (cons (string-remove-prefix "__license-" uuid) tpl)))))
    (when-let (uuid (yas-choose-value (mapcar #'car templates)))
      (yas-expand-snippet (cdr (assoc uuid templates))))))

(defun +propositum/new-project-with-templates (proj-name)
  "Creates a new propositum project & uses `yasnippet' to populate any common project files (e.g. the project org file or project scratch file)"

  (let ((proj-path (concat (car projectile-project-search-path) proj-name)))
    (progn
      (+propositum/new-project proj-name)

      ; create the main project .org file with template
      (+propositum/create-project-main-org-file (expand-file-name proj-path))

      ; TODO <project>-scratch.org

    )))

(after! magit
  (setq magit-clone-default-directory propositum-projects-directory))

(after! projectile

  ; always consider the following .dir-locals.el variables safe
  (put 'projectile-project-name 'safe-local-variable #'stringp)

  ; tell projectile where projects are stored
  (setq projectile-project-search-path '(propositum-projects-directory))

  ; create the folder if it doesn't exist already to prevent errors
  (when (not (file-exists-p (car projectile-project-search-path)))
    (make-directory (car projectile-project-search-path)))

  ; add .doom.d to known projects
  (projectile-add-known-project "~/.doom.d/")

  ; add any new projects on startup
  (projectile-discover-projects-in-search-path))

;; unique hex code depending on the project string

(defun +propositum/html-colour-code-from-string (string)
  "generate a unique, consistent HTML colour code from a given STRING"

  ; ported from: https://stackoverflow.com/a/16348977/1778342

  (let
      ((str-as-chars (string-to-list string)) ; converts each letter of the string into its UTF/char code representation
       (i 0)
       (hash 0)
       (j 0)
       (value)
       (colour (list "#")))

    ; create a hash with bitshift
    (while (< i (length string))
            (setq hash
                  (+
                   (nth i str-as-chars)
                   (-
                    (ash ; (ash x y) == << (bitwise shift left)
                     hash 5) hash)))
      (setq i (1+ i)))

    ; create the hex code
    (while (< j 3)
      (setq value
            (logand
            (ash ; (ash x (- y)) == >> (bitwise shift right)
             hash (- (* j 8)))
            #xFF))
      (push (format "%02x" value) colour) ; format string '%02x' pads out the return value with a zero where it is < 2 chars (i.e. when leading zero is stripped out)
      (setq j (1+ j)))

    ; convert the reversed list to a string and return
    (mapconcat #'identity (nreverse colour) "")))

(after! projectile
  ; mark project bg colour a safe local variable to prevent warnings
  (put 'project-bg-colour 'safe-local-variable #'stringp))

(after! projectile
  ; define a custom propositum project type
  (projectile-register-project-type 'propositum '(".propositum")
                  :run ""
                  :compile "" ))

(map!
 (:after org
   ; I use org-capture more than the scratch buffer
   :leader
   :desc "org capture" :g "x" 'org-capture
   :desc "pop scratch buffer" :g "X" 'doom/open-scratch-buffer
   ))

(after! org (use-package! org-expiry  ; Created timestamps in task
  :config
  ;; Configure timestamp property
  (setq
   org-expiry-created-property-name "CREATED" ; Name of property when an item is created
   org-expiry-inactive-timestamps   t         ; Don't have everything in the agenda view
   )

  (defun mrb/insert-created-timestamp()
    "Insert a CREATED property using org-expiry.el for TODO entries"
    (org-expiry-insert-created)
    (org-back-to-heading)
    (org-end-of-line)
    )

  ;; Whenever a TODO entry is created, I want a timestamp
  ;; Advice org-insert-todo-heading to insert a created timestamp using org-expiry
  (defadvice org-insert-todo-heading (after mrb/created-timestamp-advice activate)
    "Insert a CREATED property using org-expiry.el for TODO entries"
    (mrb/insert-created-timestamp)
    )
  ;; Make it active
  (ad-activate 'org-insert-todo-heading)))
                                       ; old fn
 ;(progn
 ;  (setq org-expiry-inactive-timestamps 't) ; Inactive to prevent unintended inclusion in other filters
 ;  (org-expiry-insinuate)))

(defun +propositum/get-project-main-org-file (&optional proj-path get-full-path)
  "Computes the name of the main .org file by deriving the `projectile-project-name' for the projectile project at PROJ-PATH.
If PROJ-PATH is nil it will use the path of the current projectile project. If GET-FULL-PATH is non-nil, returns the full, expanded path."
  ;(interactive "Dprojectile project path: "
  ;             "Dget full path?: ")
  ;; (interactive "P")
  ;; (let ((projects (projectile-relevant-known-projects)))
  ;;   (if projects
  ;;       (projectile-completing-read
  ;;        "Switch to open project: " projects
  ;;        :action (lambda (project)
  ;;                  (projectile-project-root project)))
  ;;     (user-error "There are no open projects")))
  (let*
      ((proj-path
                   (if (not proj-path)
                       (if (not (projectile-project-root))
                           (user-error "You are not in a projectile project and no project path was provided.")
                         (projectile-project-root)) ; fall-back to projectile-project-root if nil or no value provided and in project
                     (if proj-path
                         (if (not (file-directory-p proj-path))
                             (user-error "%s: File path supplied was invalid." real-this-command)
                           proj-path))))
       (proj-root (expand-file-name (file-name-as-directory proj-path)))
       (default-directory proj-root)
       (enable-local-variables :all) ; just in case they are disabled in a folder for some reason
       (proj-name ;; use a temporary buffer to load PROJ-ROOT's dir-locals variables
                    (with-temp-buffer
                      (hack-dir-local-variables-non-file-buffer)
                      projectile-project-name))
       (main-org-file))

    (when (projectile-project-p proj-root) ; make sure the directory is a projectile project
      (setq main-org-file
            (if proj-name
                (concat proj-name ".org") ; use project name as filename if it exists in .dir-locals.el
              (concat (projectile-default-project-name proj-root) ".org")))) ; otherwise use projectile default (i.e. folder name)

      (if get-full-path ; get the full path if requested
          (concat proj-root main-org-file)
        main-org-file)))

(defun +propositum/create-project-main-org-file (&optional proj-path)
  "Creates a temporary buffer, expands the default file-template/yasnippet for new projects, then saves to the path returned by `+propositum/get-project-main-org-file'.
   PROJ-PATH is the path to a projectile project, otherwise the current project will be used if we are in one.
   GET-FULL-PATH returns the full path to the new org file.

  If the project's org file already exists this function throws a user-error instead."

  (let* ((main-org-file (+propositum/get-project-main-org-file proj-path t))
        (expnd-main-org-file (expand-file-name main-org-file)))

    (when ;; don't try to expand if the project org file already exists
        (file-exists-p expnd-main-org-file)
      (user-error "'%s' already exists" (file-name-nondirectory expnd-main-org-file)))

    (with-temp-file expnd-main-org-file
      (let ((buffer-file-name expnd-main-org-file)) ;; need to set the buffer-file-name of the temp buffer otherwise errors expanding yasnippet
        (yas-minor-mode)
        (yas-expand-snippet (yas--get-template-by-uuid 'org-mode "__propositum.org"))))

    ; ensure its contents is available immediately for agendas / refiling
    (add-to-list 'org-agenda-files main-org-file)

    ))

(use-package! org-projectile
  :config
  (progn
    (org-projectile-per-project) ; strategy to use a separate .org file within each project
    (setq org-projectile-per-project-filepath '+propositum/get-project-main-org-file) ; use an .org file named <project>.org for each project
    (setq org-agenda-files (append org-agenda-files (org-projectile-todo-files))))) ; get .org file for all known projects & add to `org-agenda-files'

(after! (org projectile org-projectile)

  (setq org-capture-templates
        '(("t" "general TODO" entry ; key, description & capture type
           (file "~/org/inbox.org") ; capture target
           "* TODO %?
:PROPERTIES:
:CREATED: %U
:END:" ; template
           ; properties
           :clock-resume t
           :kill-buffer t)

          ("p" "project capture templates")
          ("pt" "current project TODO" entry
           (file (lambda () (+propositum/get-project-main-org-file nil 't)))
           "* TODO %?
:PROPERTIES:
:CREATED: %U
:END:"
           :clock-resume t
           :kill-buffer t)

          ("m" "capture MEETING notes" entry
           (file+olp+datetree (lambda () (+propositum/get-project-main-org-file nil 't)) "LOGBOOK")
           "* MEETING %u %?
:PROPERTIES:
:CREATED: %U
:ID: %(org-id-new \"meeting\")
:END:"
            :clock-in t
           :clock-resume t
           :kill-buffer t
           :time-prompt t)
          )))

(use-package! org-protocol

  :init ;; add new capture templates

  (dolist (templates
           '(("z" "[org-protocol] Capture templates")
             ("zc" "[org-protocol] Generic capture" entry ;; uses standard 'capture' sub-protocol
              (file "~/org/main.org")
              "* TODO %:description %?
:PROPERTIES:
:CREATED: %U
:ID: %(org-id-new \"meeting\")
:END:
%:annotation")
             ("zo" "[org-protocol] Generic outlook item capture" entry ;; uses custom 'outlook' sub-protocol
              (file "~/org/main.org")
              "* TODO %:subject %?
:PROPERTIES:
:CREATED: %U
:ID: %(org-id-new \"meeting\")
:END:
%:link
%:other

")
             ("zm" "[org-protocol] Capture Outlook meeting" entry ;; uses custom 'outlook' sub-protocol
              (file "~/org/main.org")
              "* %:mtgkeyword %:subject %?
:PROPERTIES:
:CREATED: %U
:ID: %(org-id-new \"meeting\")
:END:
%:timestamp
%:link
Organiser: %:sender
** Attendees
%:attendees")))
    (add-to-list 'org-capture-templates templates t))

  :config

  ;; Define a new capture protocol for outlook items

  (defun +propositum/org-protocol-capture-outlook (info)
    "Process an org-protocol://outlook style url with `INFO'.

The sub-protocol used to reach this function is set in
`org-protocol-protocol-alist'.

See also: `org-protocol-capture' for further details"

    (if (and (boundp 'org-stored-links)
             (+propositum/org-protocol-do-capture-outlook info))
        (message "Outlook item captured."))
    nil)

  ;; helper functions

  (defun +propositum/org-timestamp-range-format (start end)
    "Determines whether to use the shorter version (<yyy-MM-dd HH:mm-HH:mm>)
or longer version (<yyy-MM-dd HH:mm>--<yyyy-MM-dd HH:mm>)format
for an org-mode timestamp range, and then returns it."

    (let* ((start-as-time (org-time-string-to-time start)) ; emacs' internal representation of time
           (end-as-time (org-time-string-to-time end))
           (org-start (format-time-string (org-time-stamp-format t) start-as-time)) ; back into org format (with time) for later use
           (org-end (format-time-string (org-time-stamp-format t) end-as-time)))

      (if (string-equal (org-read-date nil nil "++0" nil start-as-time) ; confirm there is <1 day's difference
                        (org-read-date nil nil "++0" nil end-as-time))
          (concat (substring org-start 0 -1) (format-time-string "-%H:%M>" end-as-time)) ; use short-version if true
                                        ; else use the long version
        (concat org-start "--" org-end))))

  (defun +propositum/format-outlook-attendees (names &optional type)
    "Takes a string of semi-colon separated outlook display `NAMES' and outputs a string in org-mode list format.
`TYPE' should be a string specifying either 'checkbox' or 'list'. If nil, no prefix will be added."
    (let ((attendees (split-string names ";" t " ")))

      (if (string-equal type "checkbox")
          (mapconcat (lambda (x) (concat "- [ ] " x)) attendees "\n")
        (if (string-equal type "list")
            (mapconcat (lambda (x) (concat "- " x)) attendees "\n")
          (mapconcat (lambda (x) (concat "" x)) attendees "\n")))))

  (add-to-list 'org-protocol-protocol-alist
               '("org-capture-outlook" :protocol "outlook" :function +propositum/org-protocol-capture-outlook :kill-client t) t)


  ;; main function that does the work

  (defun +propositum/org-protocol-do-capture-outlook (info)
    "Capture outlook based data from INFO"
    (let* ((ol-plist (org-protocol-parse-parameters info t)) ; create plist from query string
                                        ; auxiliary variables, used to construct tokens
           (template (or (plist-get ol-plist :template)
                         org-protocol-default-template-key))
           (olid (or (plist-get ol-plist :olid) ""))
           (type (or (plist-get ol-plist :type) ""))
           (start (plist-get ol-plist :start))
           (end (plist-get ol-plist :end))
           (subject (or (plist-get ol-plist :subject) ""))
           (subject-clean (or (replace-regexp-in-string "[][]" "{" (replace-regexp-in-string "]" "}" subject)) "")) ; replace any square brackets in subject to prevent them messing up orglink
           (sender (or (plist-get ol-plist :sender) ""))
           (attendees (or (plist-get ol-plist :attendees) ""))
           (other (or (plist-get ol-plist :other) ""))
           (org-capture-link-is-already-stored t) ; avoid call to org-store-link
                                        ; variables exposed as tokens for capture templates, if doing some sort of additional transform then always pass empty string as fall-back
           (tk-link (or (org-make-link-string olid (concat type ": " subject-clean)) ""))
           (tk-timestamp (or (if (and start end) (+propositum/org-timestamp-range-format start end)) ""))
           (tk-subject subject)
           (tk-sender (or (+propositum/format-outlook-attendees sender ""))) ; clean-up the sender/organiser name
           (tk-attendees (or (+propositum/format-outlook-attendees attendees  "checkbox") "")) ; attendees as checkbox list
           (tk-meeting-keyword (if end
                                   (if (time-less-p (org-read-date t t end) (current-time)) ; check if date is in the past
                                       "MEETING"
                                     "PREP")
                                 ""))
           (tk-other other))

      (setq org-stored-links
            (cons (list olid subject-clean) org-stored-links))
      (org-store-link-props :link tk-link
                            :timestamp tk-timestamp
                            :subject tk-subject
                            :sender tk-sender
                            :attendees tk-attendees
                            :mtgkeyword tk-meeting-keyword
                            :other tk-other
                                        ;:annotation nil ; seem to be hard-coded in org-capture?
                                        ;:initial nil ; seem to be hard-coded in org-capture?
                            :query ol-plist)
      (raise-frame)
      (funcall 'org-capture nil template)))

  ;; define link export function for outlook items for org-export
  ;; source: https://lists.gnu.org/archive/html/emacs-orgmode/2018-02/msg00082.html

  (defun org-link-outlook-export-link (link desc format)
    "Create export version of LINK and DESC to FORMAT."
    (let ((link (concat "outlook:" link)))
      (cond
       ((eq format 'html)
        (format "<a href=" link desc))
       ((eq format 'latex)
        (format "\\href{%s}{%s}" link desc))
       (t                               ;`ascii', `md', `hugo', etc.
        (format "[%s](%s)" desc link)))))

  ;; add it to the list of link handlers
  (org-link-set-parameters "outlook" :export #'org-link-outlook-export-link))

(defun +propositum/project-capture-meeting ()
  "Capture a new meeting within a project (with completion)"
(interactive)
(org-projectile-project-todo-completing-read
:capture-template
"* MEETING %u %?
:PROPERTIES:
:CREATED: %U
:ID: %(org-id-new \"meeting\")
:END:"
:kill-buffer t
))

;(defvar outlook-exe-location
;  "C:/Program Files/Microsoft Office/Office16/OUTLOOK.EXE"
;  "Location of the Microsoft Outlook executable (OUTLOOK.EXE)")

(defun org-outlook-open (id)
  "Open the Outlook item identified by ID. ID should be an Outlook GUID."
  (w32-shell-execute "open" "outlook.exe" ;; outlook.exe is on the shell path, so no need for `outlook-exe-location'
    (concat "/select " "outlook:" id)))

(org-add-link-type "outlook" 'org-outlook-open)

(after! org-protocol
  (add-to-list 'org-capture-templates
             '("omp" "PREP for meeting from outlook (org-protocol)" entry
               (file+olp+datetree (lambda () (+propositum/get-project-main-org-file nil 't)) "LOGBOOK")
               "* PREP %:description %?
- [[%:link][\[OUTLOOK\] %:description]]
:PROPERTIES:
:CREATED: %U
:ID: %(org-id-new \"meeting\")
:END:") t))

;; (map!
;;  (after! org
;;  ; disable `s-x' globally
;;  "s-x" nil))

;; (after! org
;;   (global-unset-key "s-x"))
;;
(map! "s-x" nil)

;; (def-package-hook! evil
;;   :pre-config
;;   (setq evil-collection-setup-minibuffer t)
;;   nil)


(setq evil-collection-setup-minibuffer t)

(after! evil-collection
  (setq evil-collection-setup-minibuffer t))

(after! org
  (add-to-list 'org-link-abbrev-alist
               '("orange" . "https://<jira URL>/jira/<KEY>-")
               t))



(after! org
  (setq org-todo-keywords
        (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")
                (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "DELEGATED(D@/!)")
                (sequence "PREP" "|" "MEETING" "PHONE"))))

; Set the =DONE= keywords explicitly (used for filtering by =org-ql= amongst others)

  (setq org-done-keywords
        '("DONE" "CANCELLED" "DELEGATED" "MEETING" "PHONE")))

(after! org
  (setq org-todo-keyword-faces
        (quote (("TODO" :background "coral4" :foreground "coral1" :weight bold :box 1 )
                ("NEXT" :background "SpringGreen4" :foreground "SpringGreen1" :weight bold :box 1)
                ("WAITING" :background "orange4" :foreground "orange1" :weight bold :box 1)
                ("HOLD" :background "LightGoldenrod4" :foreground "LightGoldenrod1" :weight bold :box 1)
                ; DONE keywords
                ("DONE" :background "PaleGreen4" :foreground "PaleGreen1" :weight bold :box 1)
                ("CANCELLED" :background "PaleGreen4" :foreground "PaleGreen1" :weight bold :box 1)
                ("DELEGATED" :background "PaleGreen4" :foreground "PaleGreen1" :weight bold :box 1)
                ("PREP" :background "coral4" :foreground "coral1" :weight bold :box 1)
                ("MEETING" :background "SteelBlue4" :foreground "SteelBlue1" :weight bold :box 1)
                ("PHONE" :background "SteelBlue4" :foreground "SteelBlue1" :weight bold :box 1)))))



(defun +propositum/tag-org-headline (hl &rest tags)
  "Find the headline HL in the org-mode document and tag with TAGS"

  (let ((hl-pos (+propositum/find-child-headline hl))) ; get hl position

    (unless
	hl-pos
      (user-error "The headline '%s' could not be found." hl))
    (unless
	tags
      (user-error "Please provide one or more valid tags."))
    (save-excursion
      (goto-char hl-pos)
      (let ((hl-tags (org-get-tags))) ; get existing tags
	(setq hl-tags (nconc hl-tags (nreverse tags))) ; merge new & old tags
	(setq hl-tags (delq "" (delq nil (delete-dups hl-tags)))) ; remove dupes, nils and empty strings
	(org-set-tags-to hl-tags)
	(org-align-all-tags)))))

(defun +propositum/org-refile-meeting-action ()
  "Based on `org-refile' and `org-copy' but specifically for meeting actions so that the original is left in-place."
  (interactive)
  (let ((org-refile-keep t))
    (org-refile nil nil nil "Copy Meeting Action")))

(after! org
  (setq org-log-into-drawer 't))

(after! (ivy swiper counsel all-the-icons)
  (use-package! all-the-icons-ivy
  :config
  (all-the-icons-ivy-setup)))

(after! counsel
  (setq counsel-grep-base-command
 "rg -i -M 120 --no-heading --line-number --color never %s %s"))

(use-package! org-ql
  :init (org-super-agenda-mode 1) ; always load alongside `org-super-agenda'
  :config
  (setq org-super-agenda-groups nil)) ; TODO define default super-agenda groups for default agenda functions

(after! org-ql

  (defface +propositum/org-agenda-header-line
    '((t :height 200
         :inherit header-line))
    "custom face for the header-line in `org-ql-search' buffers"
    :group 'propositum)

  (set-face-attribute 'org-super-agenda-header nil
                      :font "Major Mono Display 20"
                      :overline t))

(defun +propositum/org-ql-header-custom (orig-fn &rest args)

  (let ((title (nth 2 args)))


   ; check there is a title, check the init var is bound, check it's true
   (if (and (stringp title) (boundp 'org-ql-custom-header) org-ql-custom-header)

       ; return custom header
       (propertize title 'face "+propositum/org-agenda-header-line")

    ; no custom header
    (apply orig-fn args))))

(defun +propositum/org-ql-search (&rest args)
  "Customise `org-ql-search' with a re-defined header-line and heading faces."

  ;(message "%s" args)
  (let ((org-ql-custom-header t))
    (apply #'org-ql-search args)))

(after! org-ql
  (advice-add #'org-ql-view--header-line-format :around #'+propositum/org-ql-header-custom))

(after! org-ql
  (+propositum/org-ql-search
  ;;; the location - 'where to get the raw materials (headings)'
                                        ;(org-agenda-files)
   (list "~/org/main.org") ; dbg: testing file
                                        ;(org-agenda-files)
   '(or (and (todo)
             (or
              (and
                                        ; TODO: predicate for confirming heading is outcome
                                        ; TODO: only include items who's ultimate descendant is a level 1 heading named 'OUTCOMES'
                                        ; TODO: show next 5 child items with TODO kw, of tagged outcome
               (tags "WO" "MO")) ; weekly or monthly outcome
              (tags-all "REFILE")
              (scheduled :from today :to +7) ; scheduled this  bBweek - TODO: make it 'working week'
              (deadline :from today :to +7) ; deadline this week - TODO: make it 'working week'
              ))
        (and (done)
                                        ;(or
                                        ; (property "CREATED")
                                        ;(closed -5)
             ))
                                        ;(done))


   :title " PULSE"

  ;;; the groupings - 'presenting them in a useful way'
   :super-groups
   '(
     (:name #(" due this week\n" 0 1 (rear-nonsticky t display (raise -0.24) font-lock-face (:family "Material Icons" :height 1.2) face (:family "Material Icons" :height 1.2)))
            :order 1
            :deadline t
            )

     (:name #(" outcomes\n" 0 1 (rear-nonsticky t display (raise -0.24) font-lock-face (:family "FontAwesome" :height 1.2) face (:family "FontAwesome" :height 1.2)))
            :order 2
            :tag ("WO" "MO")
            )

     (:name #(" refile\n" 0 1 (rear-nonsticky t display (raise -0.24) font-lock-face (:family "Material Icons" :height 1.2) face (:family "Material Icons" :height 1.2)))
            :order 3
            :tag "REFILE"
            )

     (:name #(" recently completed\n" 0 1 (rear-nonsticky t display (raise -0.24) font-lock-face (:family "Material Icons" :height 1.2) face (:family "Material Icons" :height 1.2)))
            :order 4
            :todo ("DONE" "MEETING")
            )
     )))

(defcustom +propositum/interaction-keywords
  (list
   "PREP"
   "MEETING"
   "PHONE")
  "List of org-mode keywords which are also classed as interactions.
   Used to distinguish interactions from other TODO keywords, e.g. in `org-super-agenda'."
  :group 'propositum)

(defun +propositum/is-interaction-p ()
  "Returns the org-mode todo keyword if org-mode element at point has a todo keyword matching `+propositum/interaction-keywords'."
  (let*
      ((hl-todo (ignore-errors
                  (substring-no-properties
                 (org-element-property
                  :todo-keyword (org-element-at-point))))))

(seq-contains +propositum/interaction-keywords hl-todo)))

(defun +propositum/find-child-headline (hl)
  "Looks for the headline `HL' within the headline at point
   and returns the buffer position, otherwise it returns `nil'"

  (let*
      ((point-on-hl (string-equal
                     hl
                     (substring-no-properties
                      (org-get-heading))))

       (point-within-hl (save-excursion
                          (progn
                            (org-back-to-heading)
                            point-on-hl))))


    (unless ;; don't return buffer position if on or within `hl'
        (or
         point-on-hl
         point-within-hl)

      (save-restriction
        (ignore-errors
          (org-narrow-to-subtree)) ;This will give error when there's no
                                        ;heading above the point, which will
                                        ;be the case for per-file post flow.
        (save-excursion
          (goto-char (point-min))

          (let (case-fold-search) ;Extracted from `org-find-exact-headline-in-buffer'
            (re-search-forward
             (format org-complex-heading-regexp-format (regexp-quote hl)) nil :noerror)))))))

(defun +propositum/is-subtree-meeting-p ()
  "Predicate to test if an org-headline represents a meeting entry.

   Returns `t' if org-mode headline has a TODO key word 'MEETING' or 'PREP'
   and has a child headline named 'Attendees'."

  (let* ((hl (org-element-at-point)) ;; get headline as element

	 (hl-context (org-element-context)) ;; get headline with extra context info

	 (hl-todo (org-element-property ;; get todo keyword of hl
		   :todo-keyword hl))

	 (hl-attendees (+propositum/find-child-headline "Attendees")))

    (if
	(and
	 (or ;; hl has one of the keywords
	  (string-equal hl-todo "MEETING")
	  (string-equal hl-todo "PREP"))
	 hl-attendees) ;; and has the attendees headline
	t
      nil)

    ))

;;; Function to find attendees list & get into correct format for ox-hugo
;; with thanks to kaushalmodi: https://github.com/kaushalmodi/ox-hugo/issues/272

(defun +propositum/hl-with-chklst-to-front-matter (hl)
  "Find the headline exactly matching HL and extract the list items into hugo fromt matter.
   Prepend with '#+HUGO_CUSTOM_FRONT_MATTER: :keyword' for a file
   or  ':EXPORT_HUGO_CUSTOM_FRONT_MATTER: :keyword' for a subtree.

The function finds  all plain list items under HL and returns as a
list \\='((checked . (VALa VALb ..)) (unchecked . (VALc VALd
..)) (transient . (VALx VALy ..)) (no-checkbox . (VALz VALq ..))

- The values in \"checked\" cons are the Org list items with
  checkbox in \"on\" [X] state.

- The value in \"unchecked\" cons are the Org list items with
 checkbox in \"unchecked\" [ ] state.

- The value in \"transient\" cons are the Org list items with
  checkbox in \"transient\" [-] checkbox state.

- The value in \"no-checkbox\" cons are the Org list items with
  no checkbox state (i.e. plain list items)."

  ;; (message "dbg x: pt: %d" (point))
  (let*
      ((hl-pos (+propositum/find-child-headline hl))
       (hl-as-element (save-restriction
                        (save-excursion
                          (goto-char hl-pos)
                          (org-narrow-to-subtree)
                          (org-element-parse-buffer))))
       checked
       unchecked
       transient
       no-checkbox
       ret)

    (org-element-map hl-as-element 'item ;Map over headline's items
      (lambda (item)
        (let* ((checkbox-state (org-element-property :checkbox item)) ;Get checkbox value of item
               (item-text (org-trim (substring-no-properties
                                     (org-element-interpret-data
                                      (org-element-contents item))))))
          (cond
           ((eq checkbox-state 'on)
            (push item-text checked))
           ((eq checkbox-state 'off)
            (push item-text unchecked))
           ((eq checkbox-state 'trans)
            (push item-text transient))
           (t ; if no checkbox present
            (push item-text no-checkbox))))))

    (setq ret ; ,@: only include in alist when item of that type is present
          `(,@(when checked `((checked . ,(nreverse checked))))
            ,@(when unchecked `((unchecked . ,(nreverse unchecked))))
            ,@(when transient `((transient . ,(nreverse transient))))
            ,@(when no-checkbox`((no-checkbox . ,(nreverse no-checkbox))))))

    ;; (message "dbg: ret: %S" ret)
    ret))

(defun +propositum/generate-hugo-post-with-fm (setup-file predicate fm-hl &optional buffer-pos)
  "With the org subtree validated by `PREDICATE' export a `hugo' markdown post with front matter using `ox-hugo' and return the full path to the generated post.

   `PREDICATE' is a function which returns 't' if the subtree is valid for export.

   `FM-HL', a headline name (without TODO keyword) within the subtree which contains org checklist items to be extracted as `hugo' front matter.

   `SETUP-FILE' is the path to a file with `ox-hugo' keyword settings. Use this to specify `hugo' variables such as content dir, section etc.

   Optionally, when `BUFFER-POS' is non-nil, move point here before evaluating `PREDICATE' and `FM-HL', otherwise the current location of point in the buffer is used."

  (unless setup-file
    (user-error "Please specify a valid `SETUP-FILE'."))

  (unless (stringp fm-hl)
    (user-error "Please specify a string representing the headline to be converted to `hugo' front matter for `FM-HL'."))

  (unless (functionp predicate)
    (user-error "`PREDICATE' must be a valid function"))

  (let
      ((orig-buffer-todo-keywords org-todo-keywords-1) ; get any keywords defined in current file for temp buffer later
       main-hl-pos
       main-hl-subtree
       main-hl-title
       err-subtree)

    (save-excursion
      (save-restriction

        (when buffer-pos ;; if buffer-pos is provided, navigate to it
          (if (not (integer-or-marker-p buffer-pos))
              (user-error "`BUFFER-POS' must be an integer or marker if supplied."))
          (goto-char buffer-pos))

        (unless
            (setq main-hl-pos
                  (catch 'point-pos
                    (when ;; If we're already on a headline matching the predicate make this the `main-hl-pos'
                        (funcall predicate)
                      (throw 'point-pos (point)))))

          ;; If we don't manage to find anything don't proceed & inform user which subtree we failed on
          (progn
            (ignore-errors (setq err-subtree (propertize (org-get-heading t nil t t) 'font-lock-face '(:foreground "red"))))
            (user-error "'%s' not a valid subtree." err-subtree)))


        ;; narrow and get buffer substring for meeting subtree
        (setq main-hl-subtree
              (progn
                (goto-char main-hl-pos)
                (org-narrow-to-subtree)
                (setq main-hl-title
                      (substring-no-properties
                       (org-get-heading t t t t)))
                (buffer-substring-no-properties (point-min) (point-max))))))

    (with-temp-buffer ; use temp buffer to avoid changing original file
      ;; (with-current-buffer (get-buffer-create (generate-new-buffer-name "temp-buffer"))  ;; temp-buffer debug
      ;;   (view-buffer-other-window (current-buffer)) ;; temp-buffer debug
      (let
          ((org-todo-keywords orig-buffer-todo-keywords)) ;; bring the original todo keywords along
        (org-mode) ; explicitly switch to org-mode to access org functions
        (insert (concat "#+SETUPFILE: " setup-file "\n"))
        (insert main-hl-subtree)
        (goto-char (point-min))
        (org-ctrl-c-ctrl-c) ; refreshes header args (pulls in setup file)
        (outline-next-heading) ; need to move to an org headline in order to execute tagging
        (+propositum/tag-org-headline fm-hl "noexport")
        (goto-char (org-find-exact-headline-in-buffer main-hl-title)) ; jump to heading
        (let ((buffer-file-name (replace-regexp-in-string " " "-" main-hl-title))) ; let-bind the identified title to buffer name so that function doesn't prompt for output file
          (org-hugo-export-to-md nil t nil))))))

(defun +propositum/generate-hugo-meeting-notes-from-subtree (&optional buffer-pos)
  "Generate meeting notes from the subtree under point using `+propositum/generate-hugo-post-with-fm'.
   Returns the path to the generated `hugo' post."

  (+propositum/generate-hugo-post-with-fm "~/.doom.d/file-templates/setup/meetings.setup" #'+propositum/is-subtree-meeting-p "Attendees" buffer-pos))

(defun +propositum/generate-all-meeting-notes-from-buffer ()
  "Generate notes from all subtrees with the `MEETING' todo keyword in the current buffer, using `org-map-entries'.
   Return a list of file paths to the generated `hugo' posts."

  (interactive)
  (org-map-entries #'+propositum/generate-hugo-meeting-notes-from-subtree "/+MEETING" nil))

(use-package! ox-pandoc

  :after ox)

(use-package! powershell

  :after org

  :config
  (add-to-list 'org-src-lang-modes '("powershell" . powershell))
  (add-to-list 'org-babel-load-languages '(powershell . t)) ; allow execution of powershell code from org-babel
  (add-to-list 'org-babel-tangle-lang-exts '("powershell" . "ps1")) ; make sure we tangle to the right extension (.ps1 not .powershell)

  ; Set the correct path to the powershell executable (tries to find the Windows one first, then the Unix one to avoid future conflicts with WSL on Win10)
  (setq powershell-location-of-exe (or (executable-find "powershell") (executable-find "pwsh")))

  :mode
  ; switch powershell mode on when we visit a .ps1 file
  ("\\.ps1\\'" . powershell-mode))

(use-package! pepita
  :config
  (setq pepita-splunk-url "") ;; replace with details of local splunk server, e.g. https://splunk.YourSplunkHost.com:8089/services/
  (setq pepita-splunk-username ""))

(after! pepita
  (use-package! ob-splunk
    :config
    (setq splunk-search-url pepita-splunk-url) ;; grab URL from pepita
    ))

(use-package! restclient)

(use-package! ob-restclient
  :after restclient
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((restclient . t))))

(def-package! uuidgen)

(use-package! outshine
  :config
  (add-hook 'emacs-lisp-mode-hook 'outshine-mode))
