;; -*- no-byte-compile: t; -*-
;;; ~/.doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:fetcher github :repo "username/repo"))
;; (package! builtin-package :disable t)


;(package! org-expiry) ; Not required because already installed?
(package! org-projectile)
(package! org-ql)
(package! powershell) ; for custom 'powershell' module
(package! lsp-pwsh :recipe (:host github :repo "kiennq/lsp-powershell")) ; for custom 'powershell' module
(package! ahk-mode)
(package! pepita) ; execute splunk queries from within emacs
(package! ob-splunk :recipe (:host github :repo "Andre0991/ob-splunk")) ; splunk integration for org-babel
(package! all-the-icons-ivy) ; add all-the-icons to common ivy file/buffer commands
(package! restclient)
(package! ob-restclient)
(package! company-restclient)
(package! uuidgen)
(package! outshine)
